from typing import Literal

import discord, requests, json
from redbot.core import commands
from redbot.core.bot import Red
from redbot.core.config import Config

RequestType = Literal["discord_deleted_user", "owner", "user", "user_strict"]


class PteroSubUser(commands.Cog):
    """
    Creates a subuser from predefined permissions
    """

    def __init__(self, bot: Red) -> None:
        self.bot = bot
        self.config = Config.get_conf(
            self,
            identifier=3621874860544622605,
            force_registration=True,
        )

        self.ids = dict()
        self.fetchIds()

    def fetchIds(self):
        headers = {'Accept': 'application/json', 'Authorization': 'Bearer '}
        r = requests.get("https://pan.selenios.co/api/application/servers", headers=headers)
        theresponse = r.json()
        for x in range(len(theresponse['data'])):
            name = theresponse['data'][x]['attributes']['name']
            id = theresponse['data'][x]['attributes']['uuid']
            self.ids[name.lower()] = id

    async def red_delete_data_for_user(self, *, requester: RequestType, user_id: int) -> None:
        # TODO: Replace this with the proper end user data removal handling.
        super().red_delete_data_for_user(requester=requester, user_id=user_id)

    @commands.has_role("⁣874295233318383647⁣")
    @commands.command()
    async def addUser(self, ctx, email: str, *, name):
        id = False
        name = name.lower()
        jsondata = {
            'email': email,
            'permissions': ['control.start', 'control.stop', 'control.restart', 'control.kill', 'user.read', 'allocation.read', 'startup.read', 'file.read', 'schedule.read', 'websocket.connect']
        }

        headers = {"Accept": "application/json", "Authorization": "Bearer "}
        try:
            id = self.ids[name]
        except Exception:
            await ctx.send("Server Not found!")
        if id:
            r = requests.post("https://pan.selenios.co/api/client/servers/" + id + "/users", headers=headers, json=jsondata)
            if r.status_code == 200:
                await ctx.send("Succeded! :tada:")
            else:
                await ctx.send(":cat_cry: Failed!")

    @commands.has_role("874295233318383647")
    @commands.command()
    async def getid(self, ctx, *, name):
        name = name.lower()
        try:
            id = self.ids[name]
            await ctx.send("ID found: " + id)
        except Exception:
            await ctx.send("No id found!")
