from redbot.core import commands, Config
from redbot.core.bot import Red
import re

from typing import Literal

RequestType = Literal["discord_deleted_user", "owner", "user", "user_strict"]

class LinkFilter(commands.Cog):

    def __init__(self, bot: Red) -> None:
        self.bot = bot

    @commands.Cog.listener()
    async def on_message(self, message):
        regex = r"((http|https)\:\/\/)?[a-zA-Z0-9\.\/\?\:@\-_=#]+\.([a-zA-Z]){2,6}([a-zA-Z0-9\.\&\/\?\:@\-_=#])*"
        url = re.findall(regex, message.content)

        if len(url) > 0:
            for role in message.author.roles:
                if role.id == 874295233318383647: # Cambiar por la de staff despues
                    return
                else:
                    await message.delete  

