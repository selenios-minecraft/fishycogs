from typing import Literal

import discord, requests, json
from redbot.core import commands
from redbot.core.bot import Red
from redbot.core.config import Config

RequestType = Literal["discord_deleted_user", "owner", "user", "user_strict"]


class Ptero(commands.Cog):
    """
    Does common Pterodactyl Functionss
    """

    def __init__(self, bot: Red) -> None:
        self.bot = bot
        self.config = Config.get_conf(
            self,
            identifier=4621874860544622605,
            force_registration=True,
        )
        self.ids = dict()
        self.fetchIds()

    def fetchIds(self):
        headers = {"Accept": "application/json", "Authorization": "Bearer T2MnEuxTQPHVJZNnmno1GQbQp4S6oPnzUCKYXKq0fqGiH1LA"}
        r = requests.get("https://pan.helioss.co/api/application/servers", headers=headers)
        theresponse = r.json()
        for x in range(len(theresponse['data'])):
            name = theresponse['data'][x]['attributes']['name']
            id = theresponse['data'][x]['attributes']['uuid']
            self.ids[name.lower()] = id


    @commands.command()
    @commands.has_role("⁣      ↑ Staff Rank ↑      ⁣")
    async def massping(self, ctx, times: int, *, message):
        user = ctx.message.mentions[0].mention
        for x in range(times):
            await ctx.send(user + " " + message + "\n")


    @commands.command()
    @commands.has_role("⁣      ↑ Staff Rank ↑      ⁣")
    async def status(self, ctx, *, server):
        headers = {"Accept": "application/json", "Authorization": "Bearer bk9IGHp4IG0zxNBp6vaM5DoyvHLkebTuwEQeCDhcbEBO3IQ6"}
        server = server.lower()
        id = False

        try:
            self.fetchIds()
            id = self.ids[server]
        except Exception:
            await ctx.send("Server Not found!")
        if id:
            r = requests.get("https://pan.helioss.co/api/client/servers/" + id + "/resources", headers=headers)
            if r.status_code == 200:
                if r.json()['attributes']['current_state'] == "running":
                    Hap = discord.utils.get(ctx.guild.emojis, name="Hap")
                    await ctx.send("Current status of " + server + ": " + r.json()['attributes']['current_state'] + f" {Hap}")
                elif r.json()['attributes']['current_state'] == "starting":
                    dab = discord.utils.get(ctx.guild.emojis, name="cat_dab")
                    await ctx.send("Current status of " + server + ": " + r.json()['attributes']['current_state'] + f" {dab}")
                else:
                    ic = discord.utils.get(ctx.guild.emojis, name="importantcustom")
                    await ctx.send("Current status of " + server + ": " + r.json()['attributes']['current_state'] + f" {ic}")
            else:
                await ctx.send(":cat_cry: Failed!")

    @commands.command()
    @commands.has_role("⁣      ↑ Staff Rank ↑      ⁣")
    async def power(self, ctx, action: str, *, server):
        server = server.lower()
        id = False
        content = {"signal": action}

        headers = {"Accept": "application/json", "Authorization": "Bearer bk9IGHp4IG0zxNBp6vaM5DoyvHLkebTuwEQeCDhcbEBO3IQ6"}
        try:
            await ctx.send(server)
            self.fetchIds()
            id = self.ids[server]
        except Exception:
            await ctx.send("Server Not found!")
        if id:
            r = requests.post("https://pan.helioss.co/api/client/servers/" + id + "/power", headers=headers, json=content)
            if r.status_code == 204:
                await ctx.send("Succeded! :tada:")
            else:
                await ctx.send(":cat_cry: Failed!")


    async def red_delete_data_for_user(self, *, requester: RequestType, user_id: int) -> None:
        # TODO: Replace this with the proper end user data removal handling.
        super().red_delete_data_for_user(requester=requester, user_id=user_id)
