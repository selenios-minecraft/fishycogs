from redbot.core import commands, events
from redbot.core.bot import Red
import mysql.connector

import discord


class Whitelist(commands.Cog):
    """Whitelist cog for Helioss"""

    def __init__(self, bot: Red) -> None:
        self.bot = bot

    @commands.command(name="white_list")
    @commands.has_role(874295233318383647)
    async def whitelist(self, ctx, args=None, username=None):
        if not args:
            await ctx.send("Available subcommands: info, add, del <username>")
        if (args == "add" or args == "info" or args == "del") and username is None:
            await ctx.send("Missing <username>")
        if args == "info" and username is not None:
            try:
                cnx = mysql.connector.connect(user='whitelist', password='F5fLFUM-x$uQ&C7*HSGr',
                                              host='94.130.53.195',
                                              database="whitelist")
                sql = """SELECT * FROM whitelist WHERE USER = %s"""

                cursor = cnx.cursor()
                cursor.execute(sql, (username, ))
                record = cursor.fetchone()

                if record is None:
                    await ctx.send(f"**{username}** is **not** in the whitelist \u274e")
                if record is not None:
                    await ctx.send(f"**{username}** is in the whitelist \u2705")
            except mysql.connector.Error as error:
                await ctx.send("Failed to insert record into MySQL table {}".format(error))
            finally:
                if cnx.is_connected():
                    cursor.close()
                    cnx.close()
        if args == "add" and username is not None:   
            try:
                cnx = mysql.connector.connect(user='whitelist', password='F5fLFUM-x$uQ&C7*HSGr',
                                              host='94.130.53.195',
                                              database="whitelist")
                sql = """INSERT INTO whitelist (user, whitelisted) VALUES (%s, %s)"""

                whitelisted = 1

                cursor = cnx.cursor()
                cursor.execute(sql, (username, whitelisted))
                cnx.commit()
                await ctx.send(f"Added **{username}** to the whitelist \u2705")
            except mysql.connector.Error as error:
                await ctx.send("Failed to insert record into MySQL table {}".format(error))
            finally:
                if cnx.is_connected():
                    cursor.close()
                    cnx.close()
        if args == "del" and username is not None:
            try:
                cnx = mysql.connector.connect(user='whitelist', password='F5fLFUM-x$uQ&C7*HSGr',
                                              host='94.130.53.195',
                                              database="whitelist")
                sql = """DELETE FROM whitelist WHERE user = %s"""

                cursor = cnx.cursor()
                cursor.execute(sql, (username, ))
                cnx.commit()
                await ctx.send(f"Removed **{username}** from the whitelist \u2705")
            except mysql.connector.Error as error:
                await ctx.send("Failed to insert record into MySQL table {}".format(error))
            finally:
                if cnx.is_connected():
                    cursor.close()
                    cnx.close()

    @commands.Cog.listener()
    async def on_message(self, message):
        channel = message.channel
        if channel.name == 'applications':
            if message.author.bot:
                return
            for role in message.author.roles:
                if role.id == 874295233318383647: # Cambiar por la de staff despues
                    return

            await message.add_reaction('\u2705')
            await message.add_reaction('\u274e')

    @commands.Cog.listener()
    async def on_reaction_add(self, reaction, user):
        if not user.bot and reaction.message.channel.id == 874297370819584060:
            channel = reaction.message.channel

            white_check_mark = discord.utils.get(user.guild.emojis, name="white_check_mark")
            sad = discord.utils.get(user.guild.emojis, name="negative_squared_cross_mark")
            green_circle = discord.utils.get(reaction.message.guild.emojis, name="green_circle")
            red_circle = discord.utils.get(reaction.message.guild.emojis, name="red_circle")
            white_check_mark = discord.utils.get(reaction.message.guild.emojis, name="white_check_mark")

            # Roles
            minecraft = discord.utils.get(reaction.message.guild.roles, name="Minecraft")
            applicant = discord.utils.get(reaction.message.guild.roles, name="Applicant")

            async for _user in reaction.users():
                for role in _user.roles:
                    if role.id == 874295233318383647:
                        if reaction.emoji == '\u2705': # Yes
                            try:
                                username = reaction.message.content.splitlines()[1].split(':')
                                user = username[1].strip()
                            except:
                                await channel.send("```Could not read the username from the application\n"
                                                   "Please add it manually```")
                            try:
                                cnx = mysql.connector.connect(user='whitelist', password='F5fLFUM-x$uQ&C7*HSGr',
                                                              host='94.130.53.195',
                                                              database="whitelist")
                                sql = """INSERT INTO whitelist (user, whitelisted) VALUES (%s, %s)"""

                                whitelisted = 1

                                cursor = cnx.cursor()
                                cursor.execute(sql, (user, whitelisted))
                                cnx.commit()
                                await reaction.message.clear_reaction('\u2705')
                                await reaction.message.clear_reaction('\u274e')
                                await reaction.message.add_reaction('\u2611')
                            except mysql.connector.Error as error:
                                await channel.send("Failed to insert record into MySQL table {}".format(error))
                            finally:
                                if cnx.is_connected():
                                    cursor.close()
                                    cnx.close()
                            await reaction.message.author.add_roles(minecraft)
                            await reaction.message.author.remove_roles(applicant)

                        if reaction.emoji == '\u274e':
                            await reaction.message.clear_reaction('\u2705')
                            await reaction.message.clear_reaction('\u274e')
                            await reaction.message.add_reaction('\u274c')
                        #if reaction.emoji == green_circle:
                        #    await reaction.message.delete()
                        #    await reaction.message.author.kick(reason="You were not accepted!")
