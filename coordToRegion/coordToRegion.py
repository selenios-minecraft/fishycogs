from typing import Literal

import discord
from redbot.core import commands
from redbot.core.bot import Red
from redbot.core.config import Config
import math

RequestType = Literal["discord_deleted_user", "owner", "user", "user_strict"]


class CoordToRegion(commands.Cog):
    """
    Does coord to region math
    """

    def coordToRegion(self, x, z):
        chunkX = math.floor(x / 16.0)
        chunkZ = math.floor(z / 16.0)

        regionX = math.floor(chunkX / 32.0)
        regionZ = math.floor(chunkZ / 32.0)
        return [regionX, regionZ]

    def __init__(self, bot: Red) -> None:
        self.bot = bot
        self.config = Config.get_conf(
            self,
            identifier=1621874860544622605,
            force_registration=True,
        )


    async def red_delete_data_for_user(self, *, requester: RequestType, user_id: int) -> None:
        # TODO: Replace this with the proper end user data removal handling.
        super().red_delete_data_for_user(requester=requester, user_id=user_id)

    @commands.command()
    async def ctr(self, ctx, x: str, z: str):
        """Coord to Region"""
        xInt = int(x)
        zInt = int(z)
        regionX, regionZ = self.coordToRegion(xInt, zInt)
        await ctx.send("Region file: r." + str(regionX) + "." + str(regionZ) + ".mca")
