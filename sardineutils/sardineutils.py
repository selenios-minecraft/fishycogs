from typing import Literal

import discord, os, textwrap, io
import redbot
from redbot.core import commands
from redbot.core.bot import Red
from redbot.core.config import Config
import os
import signal
import subprocess

RequestType = Literal["discord_deleted_user", "owner", "user", "user_strict"]


class SardineUtils(commands.Cog):
    """
    Weird Utilities :P
    """

    def cleanup_code(self, content):
        """Automatically removes code blocks from the code."""
        # remove ```py\n```
        if content.startswith('```') and content.endswith('```'):
            return '\n'.join(content.split('\n')[1:-1])
        # remove `foo`
        return content.strip('` \n')

    def __init__(self, bot: Red) -> None:
        self.bot = bot
        self.config = Config.get_conf(
            self,
            identifier=12621874860544622605,
            force_registration=True,
        )

#    @commands.command()
#    @redbot.core.checks.is_owner()
#    async def shell(self, ctx, *, command):
#        await ctx.send("```\n" + os.popen(self.cleanup_code(command)).read() + "```")

    @commands.command()
    async def bn(self, ctx, emoji):
        emoji = discord.utils.get(ctx.guild.emojis, name=emoji)
        await ctx.send(emoji)


    @commands.command()
    async def runchestbot(self, ctx, ip, port, x, y, z, item, quantity):
        """DOES NOT SUPPORT NBT YET, COMING SOON"""
        os.system("/home/linuxbrew/.linuxbrew/bin/node /home/sardine/trydeliverchest.js " + ip + " " + port + " " + x + " " + y + " " + z + " " + item + " " + quantity)
        await ctx.send("Launched the bot! please wait, it takes a little while.")

    @commands.command()
    async def runquestbot(self, ctx, ip, port, player, filename, *, ids):
        f = open("/home/sardine/questfiles/" + filename, "w")
        f.write(ids)
        f.close()
        os.system("/home/linuxbrew/.linuxbrew/bin/node /home/sardine/questbot.js " + ip + " " + port + " /home/sardine/questfiles/" + filename + " " + player)
        await ctx.send("Launched the bot! Please wait, it needs to wait 0.5 seconds per id.")




    @commands.command()
    @redbot.core.checks.is_owner()
    async def lolcode(self, ctx, *, code):
        code = self.cleanup_code(code)
        stdout = io.StringIO()
        try:
            to_compile = f'\"HAI 1.4\n{code}\nKTHXBYE\"'
        except Exception as e:
            await ctx.send("Error: " + str(e))
            return

        output = os.popen("printf " + to_compile + " | /home/linuxbrew/.linuxbrew/bin/lci -").read()
        await ctx.send(f"```\n{output}\n```")



    async def red_delete_data_for_user(self, *, requester: RequestType, user_id: int) -> None:
        # TODO: Replace this with the proper end user data removal handling.
        super().red_delete_data_for_user(requester=requester, user_id=user_id)
