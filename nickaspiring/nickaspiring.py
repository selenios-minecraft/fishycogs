from typing import Literal

import discord
from redbot.core import commands
from redbot.core.bot import Red
from redbot.core.config import Config
import asyncio
import time, random
import sys
import psutil
import os
from discord.ext import tasks

RequestType = Literal["discord_deleted_user", "owner", "user", "user_strict"]


class NickAspiring(commands.Cog):
    """
    Nicks Aspiring
    """

    def __init__(self, bot: Red) -> None:
        self.bot = bot
        self.config = Config.get_conf(
            self,
            identifier=642430396683911187,
            force_registration=True,
        )
        #self.getNicks()
        self.nicknames = ("Polypropylene",
                             "Polyvinyl-Chloride",
                             "Polyvinylidenechloride",
                             "Polyacrylonitrile",
                             "Polytetrafluoroethylene",
                             "Polymethylmethacrylate",
                             "Polyvinylacetate",
                             "Cis-Polyisoprene",
                             "Polychloroprene",
                             "Polyurethane",
                             "Polybenzimidazole",
                             "Polyacetal",
                             "Polyacrylonitrile",
                             "Polybutadiene",
                             "Polybutylene-Terephthalate",
                             "Polycaprolactam",
                             "Polycaprolactam",
                             "Polychlorotrifluoroethylene",
                             "Polycyclohexyl-Methacrylate",
                             "Polycopractom",
                             "Polydimethylsiloxane",
                             "Polydodecano-12-Lactam",
                             "Polyether-Ether-Ketone",
                             "Polyether-Ketone-Ketone",
                             "Polyethersulfone",
                             "Polyethyl-Acrylate",
                             "Polyethylene-Glycol",
                             "Polyethylene-Naphthalate",
                             "Polyethylene-terephthalate)",
                             "Polyhexamethylene-Adipamide",)
        self.nick.start()


    # def setDefaultNicks(self, names: tuple):
    #     c = self.bot.database.cursor()
    #     for x in range(len(names)):
    #         name = names[x]
    #         c.execute(f"INSERT or IGNORE INTO aspiringnames (guild, name) VALUES (?, ?)", (602313280702382106, name))
    #     self.bot.database.commit()
    #     c.close()
    #
    # def insertNick(self, name: str):
    #     c = self.bot.database.cursor()
    #     c.execute(f"INSERT or IGNORE INTO aspiringnames (guild, name) VALUES (?, ?)", (602313280702382106, name))
    #     self.nicknames.append(name)
    #     self.bot.database.commit()
    #     c.close()
    #
    # @commands.command(name='addnick')
    # async def addnick(self, ctx: commands.Context, nick: str):
    #     self.insertNick(nick)
    #     await ctx.send("Done! Added " + nick + " to the database.")


    # def getNicks(self):
    #     c = self.bot.database.cursor()
    #     for row in c.execute('SELECT * FROM aspiringnames'):
    #         self.nicknames.append(row[1])
    #     c.close()

    @tasks.loop(hours=4)
    async def nick(self):
        aspiring = None
        for guild in self.bot.guilds:
            for member in guild.members:
                if member.id == 642430396683911187:
                    aspiring = member
        await aspiring.edit(nick="Aspiring " + self.nicknames[random.randint(0, len(self.nicknames) - 1)])

    async def red_delete_data_for_user(self, *, requester: RequestType, user_id: int) -> None:
        # TODO: Replace this with the proper end user data removal handling.
        super().red_delete_data_for_user(requester=requester, user_id=user_id)
