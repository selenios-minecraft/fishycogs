
from nbtlib import Byte
import nbtlib
import requests, os
import json
# from mcuuid.api import GetPlayerData

def getUUID(playername):
    r = requests.get("https://playerdb.co/api/player/minecraft/" + playername)
    loads = json.loads(r.content)
    if loads['data']['player']['id']:
        return loads['data']['player']['id']
    else:
        return False
    # data = GetPlayerData(playername)
    # if data.valid:
    #     return data.uuid
    # else:
    #     return False
