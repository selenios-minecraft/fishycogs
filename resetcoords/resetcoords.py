from typing import Literal

import discord, nbtlib, os
from nbtlib import *
from redbot.core import commands
from redbot.core.bot import Red
from redbot.core.config import Config
from .utils import *

RequestType = Literal["discord_deleted_user", "owner", "user", "user_strict"]


class ResetCoords(commands.Cog):
    """
    Resets coords
    """

    def __init__(self, bot: Red) -> None:
        self.bot = bot
        self.config = Config.get_conf(
            self,
            identifier=734049639765901352,
            force_registration=True,
        )
        self.ids = dict()
        self.fetchIds()

    def fetchIds(self):
        headers = {"Accept": "application/json", "Authorization": "Bearer JxftsQK8anCy8xN4nGxbhYGBdDLadIFwKEmrceLJlCfIcdkO"}
        r = requests.get("https://pan.selenios.co/api/application/servers", headers=headers)
        theresponse = r.json()
        for x in range(len(theresponse['data'])):
            name = theresponse['data'][x]['attributes']['name']
            id = theresponse['data'][x]['attributes']['identifier']
            self.ids[name.lower()] = id
    
    @commands.command()
    async def resetcoords(self, ctx, player: str, xCoord: int, yCoord: int, zCoord: int, dim: int, *, server):
        uuid = getUUID(player)
        headers = {"Accept": "application/json", "Authorization": "Bearer aaKMBckgcJBRv0OyfV6xaihjRhyLCRLRuExUmwdvXuZAOKru"}
        server = server.lower()
        id = False

        try:
            self.fetchIds()
            id = self.ids[server]
        except Exception:
            await ctx.send("Server Not found!")
        if id and uuid:
            onetimerequest = requests.get("https://pan.selenios.co/api/client/servers/" + id + "/files/download?file=%2F/world/playerdata/" + uuid + ".dat", headers=headers)
            if onetimerequest.status_code != 200:
                await ctx.send(onetimerequest.status_code)
                await ctx.send("Failed! Contact FranT4NK...")
                return
            theresponse = onetimerequest.json()
            url = theresponse["attributes"]["url"]
            r = requests.get(url, allow_redirects=True, headers=headers)
            await ctx.send(r.content)

            if r.status_code != 200:
                await ctx.send("Failed! Contact FranT4NK...")
                return

            open("/home/FranT4NK/tempfiles/" + uuid + ".dat", 'wb').write(r.content)
            await ctx.send("Successfully downloaded NBT file!")
            nbtfile = nbtlib.load("/home/FranT4NK/tempfiles/" + uuid + ".dat")
            await ctx.send("Resetting " + player + "\'s coordinates to " + str(xCoord) + "," + str(yCoord) + "," + str(zCoord) + "...")
            nbtfile.root["Pos"][0] = xCoord
            nbtfile.root["Pos"][1] = yCoord
            nbtfile.root["Pos"][2] = zCoord
            nbtfile.root["Dimension"] = Int(dim)
            nbtfile.save()
            await ctx.send("Uploading to server...")
            onetimerequest = requests.get("https://pan.selenios.co/api/client/servers/" + id + "/files/upload", headers=headers)
            await ctx.send(onetimerequest.json())
            if onetimerequest.status_code != 200:
                await ctx.send("Failed! Contact FranT4NK...")
                return
            theresponse = onetimerequest.json()
            url = theresponse["attributes"]["url"]
            files = {'file': open("/home/FranT4NK/tempfiles/" + uuid + ".dat", 'rb')}
            r = requests.post(url, files=files, allow_redirects=True, headers=headers)
            if r.status_code == 200:
                await ctx.send("Successfully uploaded! :tada:")
            else:
                await ctx.send(r.text)
                await ctx.send("Failed! Contact FranT4NK...")
            
            os.unlink("/home/FranT4NK/tempfiles/" + uuid + ".dat")


    async def red_delete_data_for_user(self, *, requester: RequestType, user_id: int) -> None:
        # TODO: Replace this with the proper end user data removal handling.
        super().red_delete_data_for_user(requester=requester, user_id=user_id)
