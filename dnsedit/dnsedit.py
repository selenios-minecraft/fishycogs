from typing import Literal

import discord
from redbot.core import commands
from redbot.core.bot import Red
from redbot.core.config import Config
from godaddypy import Client, Account

RequestType = Literal["discord_deleted_user", "owner", "user", "user_strict"]


class DnsEdit(commands.Cog):
    """
    Edits DNS on GoDaddy
    """

    def __init__(self, bot: Red) -> None:
        self.bot = bot
        self.config = Config.get_conf(
            self,
            identifier=15824512284687430123,
            force_registration=True,
        )
        self.client = Client(Account(api_key='e4hfgFgBqtRP_2bE38ZaM4pFCyLTkyfwc8g', api_secret='PuQkVGircXtjQ2jocKcwjb'))

    @commands.command()
    @commands.has_any_role(738119274115235901, 625791697556733984)
    async def arecord(self, ctx, ip: str, subdomain: str):
        addrecorda = self.client.add_record('helioss.co', {'data': ip, 'name': subdomain, 'ttl': 3600, 'type': 'A'})
        if addrecorda:
            await ctx.send("Succeded! :tada:")

    @commands.command()
    @commands.has_any_role(738119274115235901, 625791697556733984)
    async def setupdns(self, ctx, ip: str, subdomain: str, port: int):
        addrecorda = self.client.add_record('helioss.co', {'data': ip, 'name': subdomain, 'ttl': 3600, 'type': 'A'})
        addrecordb = self.client.add_record('helioss.co', {'data': subdomain + '.helioss.co', 'name': subdomain, 'port': port, 'priority': 0, 'protocol': '_minecraft', 'service': '_tcp', 'ttl': 3600, 'type': 'SRV', 'weight': 5})
        if addrecorda and addrecordb:
            await ctx.send("Succeded! :tada:")

    async def red_delete_data_for_user(self, *, requester: RequestType, user_id: int) -> None:
        # TODO: Replace this with the proper end user data removal handling.
        super().red_delete_data_for_user(requester=requester, user_id=user_id)
