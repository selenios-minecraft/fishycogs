from typing import Literal

import discord
import hashlib
from redbot.core import commands
from redbot.core.bot import Red
from redbot.core.config import Config

RequestType = Literal["discord_deleted_user", "owner", "user", "user_strict"]

class HashCog(commands.Cog):
    """
    String to md5 hash
    """

    def __init__(self, bot: Red) -> None:
        self.bot = bot
        self.config = Config.get_conf(
            self,
            identifier=6621874860544622605,
            force_registration=True,
        )

    @commands.command()
    async def md5hash(self, ctx, *, thing):
        await ctx.send(hashlib.md5(thing.encode()).hexdigest())

    @commands.command()
    async def sha256hash(self, ctx, *, thing):
        await ctx.send(hashlib.sha256(thing.encode()).hexdigest())

    @commands.command()
    async def sha1hash(self, ctx, *, thing):
        await ctx.send(hashlib.sha1(thing.encode()).hexdigest())

    @commands.command()
    async def sha512hash(self, ctx, *, thing):
        await ctx.send(hashlib.sha512(thing.encode()).hexdigest())



    async def red_delete_data_for_user(self, *, requester: RequestType, user_id: int) -> None:
        # TODO: Replace this with the proper end user data removal handling.
        super().red_delete_data_for_user(requester=requester, user_id=user_id)
