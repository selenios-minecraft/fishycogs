from typing import Literal

import discord, requests, json
from redbot.core import commands
from redbot.core.bot import Red
from redbot.core.config import Config

RequestType = Literal["discord_deleted_user", "owner", "user", "user_strict"]


class GetUUID(commands.Cog):
    """
    Gets UUID of player from Mojang's API
    """

    def getUUID(self, playername):
        r = requests.get("https://playerdb.co/api/player/minecraft/" + playername)
        loads = json.loads(r.content)
        if loads['data']['player']['id']:
            return loads['data']['player']['id']
        else:
            return False
        # data = GetPlayerData(playername)
        # if data.valid:
        #     return data.uuid
        # else:
        #     return False


    def __init__(self, bot: Red) -> None:
        self.bot = bot
        self.config = Config.get_conf(
            self,
            identifier=2621874860544622605,
            force_registration=True,
        )

    async def red_delete_data_for_user(self, *, requester: RequestType, user_id: int) -> None:
        # TODO: Replace this with the proper end user data removal handling.
        super().red_delete_data_for_user(requester=requester, user_id=user_id)

    @commands.command()
    async def uuid(self, ctx, player: str):
        uuid = self.getUUID(player)
        if uuid:
            await ctx.send("UUID Found: " + uuid)
        else:
            await ctx.send("No UUID found!")
